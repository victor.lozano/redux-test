[![pipeline status](https://gitlab.com/bardux/redux-test/badges/master/pipeline.svg)](https://gitlab.com/bardux/redux-test/commits/master)

# Redux test

## Pages
<https://bardux.gitlab.io/redux-test/>

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
webpack --watch
```
