import {createStore, combineReducers} from 'redux'
require('./styles.scss')

let input = document.getElementById("input")
let inputEmail = document.getElementById("input-email")
let listElement = document.getElementById("list")
let listEmailsElement = document.getElementById("list-emails")

const drawTodos = _ => {
    listElement.innerHTML = ''
    const todos = store.getState().todos
    todos.map(todo => {
        let div = document.createElement('div')
        div.id = todo.id
        let classDone = todo.done ? 'done' : ''
        div.innerHTML = `<div data-id="${todo.id}" class="list-item">
        <span data-id="${todo.id}" class="${classDone}">${todo.text}</span>
        <span data-id="${todo.id}" data-action="delete" class="button is-small is-danger">X</span>
        </div>`
        setListeners(div)
        listElement.appendChild(div)
    })
}

const drawEmails = _ => {
    listEmailsElement.innerHTML = ''
    const emails = store.getState().emails
    for(let key in emails) {
        let div = document.createElement('div')
        div.id = key
        div.innerHTML = `<div class="list-item">
        <span>${emails[key]}</span>
        <span data-id="${key}" class="button is-small is-danger">X</span>
        </div>`
        setEmailListeners(div)
        listEmailsElement.appendChild(div)
    }
}

const setListeners = span => {
    span.addEventListener('click', e => {
        const key = parseInt(e.target.getAttribute('data-id'))
        const todos = store.getState().todos
        store.dispatch({
            type: e.target.getAttribute('data-action') === 'delete'? 'DELETE_TODO' : 'UPDATE_TODO',
            todo: todos[key]
        })
    })
}

const setEmailListeners = span => {
    span.addEventListener('click', e => {
        let key = e.target.getAttribute('data-id')
        if(key) {
            key = parseInt(key)
            store.dispatch({
                type: 'DELETE_EMAIL',
                key
            })
        }
    })
}

input.addEventListener('keydown', e => {
    if(e.key === "Enter") {
        const text = e.target.value
        const todo = {text, done: false}
        store.dispatch({
            type: 'CREATE_TODO',
            todo
        })
        e.target.value = ''
    }
})

inputEmail.addEventListener('keydown', e => {
    if(e.key === "Enter") {
        const email = e.target.value
        store.dispatch({
            type: 'CREATE_EMAIL',
            email
        })
        e.target.value = ''
    }
})

const todosReducer = (state = [], action) => {
    switch(action.type) {
        case 'CREATE_TODO':
            action.todo.id = Object.keys(state).length
            return [...state, action.todo]
        case 'UPDATE_TODO':
            action.todo.done = !action.todo.done
            state[action.todo.id] = action.todo
            return state
        case 'DELETE_TODO':
            return [...state.filter(todo => todo !== action.todo)]
        default:
            return state
    }
}

const emailReducer = (state = {}, action) => {
    switch(action.type) {
        case 'CREATE_EMAIL':
            return {...state, [Object.keys(state).length]: action.email}
        case 'DELETE_EMAIL':
            delete state[action.key]
            return state
        default:
            return state
    }
}

const rootReducer = combineReducers({
    todos: todosReducer,
    emails: emailReducer
})

let store = createStore(rootReducer, {
    todos: [],
    emails: {}
})

store.subscribe(_ => {
    drawTodos()
    drawEmails()
})
