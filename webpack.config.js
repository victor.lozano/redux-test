let path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
    entry: "./main.js",
    mode: "production",
    output: {
        path: path.join(__dirname, "build"),
        filename: "main.js"
    },
    module: {
      rules: [{
        test: /\.scss$/,
        use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader'
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
                // options...
              }
            }
          ]
      }]
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'css/[name].bundle.css'
      }),
    ]
}